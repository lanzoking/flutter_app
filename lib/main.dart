import 'package:flutter/material.dart';

const String bodyText = "Welcome to educator";
const imageURI = "assets/penguins.jpg";
const String floatingActionButtonText = "click";
const Color currentColor = Colors.black38;
var  textColor = Colors.grey[700];

void main() => runApp(MaterialApp(
  home: Home(),
));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Educator"),
        centerTitle: true,
        backgroundColor: currentColor,
      ),
      body: Center(
        child: Text(
         bodyText,
        style: TextStyle(
          color: currentColor,
          fontWeight: FontWeight.bold,
        ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Text(floatingActionButtonText),
        backgroundColor: currentColor,
      ),
    );
  }
}
